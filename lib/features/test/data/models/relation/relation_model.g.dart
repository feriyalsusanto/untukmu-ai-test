// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'relation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RelationModelImpl _$$RelationModelImplFromJson(Map<String, dynamic> json) =>
    _$RelationModelImpl(
      name: json['name'] as String,
      category: json['category'] as String,
    );

Map<String, dynamic> _$$RelationModelImplToJson(_$RelationModelImpl instance) =>
    <String, dynamic>{
      'name': instance.name,
      'category': instance.category,
    };
