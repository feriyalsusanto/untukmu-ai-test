import 'package:freezed_annotation/freezed_annotation.dart';

part 'relation_model.freezed.dart';
part 'relation_model.g.dart';

@Freezed()
class RelationModel with _$RelationModel {
  const factory RelationModel({
    required String name,
    required String category,
  }) = _RelationModel;

  factory RelationModel.fromJson(Map<String, Object?> json) =>
      _$RelationModelFromJson(json);
}
