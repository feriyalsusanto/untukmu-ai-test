// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hero_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$HeroModelImpl _$$HeroModelImplFromJson(Map<String, dynamic> json) =>
    _$HeroModelImpl(
      id: (json['id'] as num).toInt(),
      nickname: json['nickname'] as String?,
      relations: json['relations'] as String?,
      photoPath: json['photoPath'] as String?,
    );

Map<String, dynamic> _$$HeroModelImplToJson(_$HeroModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nickname': instance.nickname,
      'relations': instance.relations,
      'photoPath': instance.photoPath,
    };
