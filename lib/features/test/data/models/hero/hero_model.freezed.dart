// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'hero_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

HeroModel _$HeroModelFromJson(Map<String, dynamic> json) {
  return _HeroModel.fromJson(json);
}

/// @nodoc
mixin _$HeroModel {
  int get id => throw _privateConstructorUsedError;
  String? get nickname => throw _privateConstructorUsedError;
  String? get relations => throw _privateConstructorUsedError;
  String? get photoPath => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $HeroModelCopyWith<HeroModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HeroModelCopyWith<$Res> {
  factory $HeroModelCopyWith(HeroModel value, $Res Function(HeroModel) then) =
      _$HeroModelCopyWithImpl<$Res, HeroModel>;
  @useResult
  $Res call({int id, String? nickname, String? relations, String? photoPath});
}

/// @nodoc
class _$HeroModelCopyWithImpl<$Res, $Val extends HeroModel>
    implements $HeroModelCopyWith<$Res> {
  _$HeroModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? nickname = freezed,
    Object? relations = freezed,
    Object? photoPath = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      nickname: freezed == nickname
          ? _value.nickname
          : nickname // ignore: cast_nullable_to_non_nullable
              as String?,
      relations: freezed == relations
          ? _value.relations
          : relations // ignore: cast_nullable_to_non_nullable
              as String?,
      photoPath: freezed == photoPath
          ? _value.photoPath
          : photoPath // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HeroModelImplCopyWith<$Res>
    implements $HeroModelCopyWith<$Res> {
  factory _$$HeroModelImplCopyWith(
          _$HeroModelImpl value, $Res Function(_$HeroModelImpl) then) =
      __$$HeroModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String? nickname, String? relations, String? photoPath});
}

/// @nodoc
class __$$HeroModelImplCopyWithImpl<$Res>
    extends _$HeroModelCopyWithImpl<$Res, _$HeroModelImpl>
    implements _$$HeroModelImplCopyWith<$Res> {
  __$$HeroModelImplCopyWithImpl(
      _$HeroModelImpl _value, $Res Function(_$HeroModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? nickname = freezed,
    Object? relations = freezed,
    Object? photoPath = freezed,
  }) {
    return _then(_$HeroModelImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      nickname: freezed == nickname
          ? _value.nickname
          : nickname // ignore: cast_nullable_to_non_nullable
              as String?,
      relations: freezed == relations
          ? _value.relations
          : relations // ignore: cast_nullable_to_non_nullable
              as String?,
      photoPath: freezed == photoPath
          ? _value.photoPath
          : photoPath // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$HeroModelImpl implements _HeroModel {
  const _$HeroModelImpl(
      {required this.id, this.nickname, this.relations, this.photoPath});

  factory _$HeroModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$HeroModelImplFromJson(json);

  @override
  final int id;
  @override
  final String? nickname;
  @override
  final String? relations;
  @override
  final String? photoPath;

  @override
  String toString() {
    return 'HeroModel(id: $id, nickname: $nickname, relations: $relations, photoPath: $photoPath)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HeroModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.nickname, nickname) ||
                other.nickname == nickname) &&
            (identical(other.relations, relations) ||
                other.relations == relations) &&
            (identical(other.photoPath, photoPath) ||
                other.photoPath == photoPath));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, nickname, relations, photoPath);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HeroModelImplCopyWith<_$HeroModelImpl> get copyWith =>
      __$$HeroModelImplCopyWithImpl<_$HeroModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$HeroModelImplToJson(
      this,
    );
  }
}

abstract class _HeroModel implements HeroModel {
  const factory _HeroModel(
      {required final int id,
      final String? nickname,
      final String? relations,
      final String? photoPath}) = _$HeroModelImpl;

  factory _HeroModel.fromJson(Map<String, dynamic> json) =
      _$HeroModelImpl.fromJson;

  @override
  int get id;
  @override
  String? get nickname;
  @override
  String? get relations;
  @override
  String? get photoPath;
  @override
  @JsonKey(ignore: true)
  _$$HeroModelImplCopyWith<_$HeroModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
