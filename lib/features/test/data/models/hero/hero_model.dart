import 'package:freezed_annotation/freezed_annotation.dart';

part 'hero_model.freezed.dart';
part 'hero_model.g.dart';

@Freezed()
class HeroModel with _$HeroModel {
  const factory HeroModel({
    required int id,
    String? nickname,
    String? relations,
    String? photoPath,
  }) = _HeroModel;

  factory HeroModel.fromJson(Map<String, Object?> json) =>
      _$HeroModelFromJson(json);
}
