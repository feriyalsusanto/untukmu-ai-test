part of 'test_cubit.dart';

enum Status {
  initialize,
  updating,
  updated,
}

@Freezed()
class TestState with _$TestState {
  const factory TestState({
    @Default(Status.initialize) status,
    List<HeroModel>? heroes,
  }) = _TestState;
}
