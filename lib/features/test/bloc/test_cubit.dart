import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:untukmu_ai_mobile/features/test/data/models/hero/hero_model.dart';

part 'test_cubit.freezed.dart';
part 'test_state.dart';

class TestCubit extends Cubit<TestState> {
  TestCubit() : super(const TestState());

  void initialize() {
    emit(state.copyWith(status: Status.updating));

    List<HeroModel> heroes =
        List.generate(3, (index) => HeroModel(id: (index + 1)), growable: true);

    emit(state.copyWith(status: Status.updated, heroes: heroes));
  }

  void update(int index,
      {String? nickname, String? photoPath, String? relations}) {
    emit(state.copyWith(status: Status.updating));

    var hero = state.heroes!.elementAt(index);
    var heroUpdate = HeroModel(
        id: hero.id,
        nickname: nickname ?? hero.nickname,
        photoPath: photoPath ?? hero.photoPath,
        relations: relations ?? hero.relations);
    List<HeroModel> heroes =
        List.generate(state.heroes?.length ?? 0, (position) {
      if (index == position) {
        return heroUpdate;
      } else {
        return state.heroes![position];
      }
    });

    emit(state.copyWith(status: Status.updated, heroes: heroes));
  }

  Future<void> getPicture(int index) async {
    final ImagePicker picker = ImagePicker();
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      update(index, photoPath: image.path);
    }
  }
}
