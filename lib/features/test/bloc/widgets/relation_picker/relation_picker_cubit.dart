import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:untukmu_ai_mobile/features/test/data/models/relation/relation_model.dart';

part 'relation_picker_cubit.freezed.dart';
part 'relation_picker_state.dart';

class RelationPickerCubit extends Cubit<RelationPickerState> {
  RelationPickerCubit() : super(const RelationPickerState());

  initialize() {
    emit(state.copyWith(status: Status.initializing));

    List<RelationModel> families = [
      const RelationModel(name: 'Father', category: 'Family'),
      const RelationModel(name: 'Mother', category: 'Family'),
      const RelationModel(name: 'Grand-Father', category: 'Family'),
      const RelationModel(name: 'Grand-Mother', category: 'Family'),
      const RelationModel(name: 'Sister', category: 'Family'),
      const RelationModel(name: 'Cousin', category: 'Family'),
      const RelationModel(name: 'Brother', category: 'Family'),
      const RelationModel(name: 'Uncle', category: 'Family'),
      const RelationModel(name: 'Aunt', category: 'Family'),
      const RelationModel(name: 'Niece', category: 'Family'),
      const RelationModel(name: 'Grand-Child', category: 'Family'),
    ];
    List<RelationModel> friends = [
      const RelationModel(name: 'Friend', category: 'Friends & Connections'),
      const RelationModel(
          name: 'Best-Friend', category: 'Friends & Connections'),
      const RelationModel(name: 'Coworker', category: 'Friends & Connections'),
    ];

    Map<String, List<RelationModel>> relations = {
      'Family': families,
      'Friends & Connections': friends
    };

    emit(state.copyWith(status: Status.initialize, relations: relations));
  }

  void selectItem(RelationModel relation) {
    emit(state.copyWith(
        status: Status.selected, selectedRelation: relation.name));
  }
}
