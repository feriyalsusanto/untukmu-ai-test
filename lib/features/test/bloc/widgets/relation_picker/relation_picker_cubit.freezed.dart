// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'relation_picker_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$RelationPickerState {
  Status get status => throw _privateConstructorUsedError;
  Map<String, List<RelationModel>>? get relations =>
      throw _privateConstructorUsedError;
  String? get selectedRelation => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RelationPickerStateCopyWith<RelationPickerState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RelationPickerStateCopyWith<$Res> {
  factory $RelationPickerStateCopyWith(
          RelationPickerState value, $Res Function(RelationPickerState) then) =
      _$RelationPickerStateCopyWithImpl<$Res, RelationPickerState>;
  @useResult
  $Res call(
      {Status status,
      Map<String, List<RelationModel>>? relations,
      String? selectedRelation});
}

/// @nodoc
class _$RelationPickerStateCopyWithImpl<$Res, $Val extends RelationPickerState>
    implements $RelationPickerStateCopyWith<$Res> {
  _$RelationPickerStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? relations = freezed,
    Object? selectedRelation = freezed,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as Status,
      relations: freezed == relations
          ? _value.relations
          : relations // ignore: cast_nullable_to_non_nullable
              as Map<String, List<RelationModel>>?,
      selectedRelation: freezed == selectedRelation
          ? _value.selectedRelation
          : selectedRelation // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RelationPickerStateImplCopyWith<$Res>
    implements $RelationPickerStateCopyWith<$Res> {
  factory _$$RelationPickerStateImplCopyWith(_$RelationPickerStateImpl value,
          $Res Function(_$RelationPickerStateImpl) then) =
      __$$RelationPickerStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Status status,
      Map<String, List<RelationModel>>? relations,
      String? selectedRelation});
}

/// @nodoc
class __$$RelationPickerStateImplCopyWithImpl<$Res>
    extends _$RelationPickerStateCopyWithImpl<$Res, _$RelationPickerStateImpl>
    implements _$$RelationPickerStateImplCopyWith<$Res> {
  __$$RelationPickerStateImplCopyWithImpl(_$RelationPickerStateImpl _value,
      $Res Function(_$RelationPickerStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? relations = freezed,
    Object? selectedRelation = freezed,
  }) {
    return _then(_$RelationPickerStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as Status,
      relations: freezed == relations
          ? _value._relations
          : relations // ignore: cast_nullable_to_non_nullable
              as Map<String, List<RelationModel>>?,
      selectedRelation: freezed == selectedRelation
          ? _value.selectedRelation
          : selectedRelation // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$RelationPickerStateImpl implements _RelationPickerState {
  const _$RelationPickerStateImpl(
      {this.status = Status.initialize,
      final Map<String, List<RelationModel>>? relations,
      this.selectedRelation})
      : _relations = relations;

  @override
  @JsonKey()
  final Status status;
  final Map<String, List<RelationModel>>? _relations;
  @override
  Map<String, List<RelationModel>>? get relations {
    final value = _relations;
    if (value == null) return null;
    if (_relations is EqualUnmodifiableMapView) return _relations;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(value);
  }

  @override
  final String? selectedRelation;

  @override
  String toString() {
    return 'RelationPickerState(status: $status, relations: $relations, selectedRelation: $selectedRelation)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RelationPickerStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality()
                .equals(other._relations, _relations) &&
            (identical(other.selectedRelation, selectedRelation) ||
                other.selectedRelation == selectedRelation));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status,
      const DeepCollectionEquality().hash(_relations), selectedRelation);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RelationPickerStateImplCopyWith<_$RelationPickerStateImpl> get copyWith =>
      __$$RelationPickerStateImplCopyWithImpl<_$RelationPickerStateImpl>(
          this, _$identity);
}

abstract class _RelationPickerState implements RelationPickerState {
  const factory _RelationPickerState(
      {final Status status,
      final Map<String, List<RelationModel>>? relations,
      final String? selectedRelation}) = _$RelationPickerStateImpl;

  @override
  Status get status;
  @override
  Map<String, List<RelationModel>>? get relations;
  @override
  String? get selectedRelation;
  @override
  @JsonKey(ignore: true)
  _$$RelationPickerStateImplCopyWith<_$RelationPickerStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
