part of 'relation_picker_cubit.dart';

enum Status {
  initializing,
  initialize,
  selected,
}

@Freezed()
class RelationPickerState with _$RelationPickerState {
  const factory RelationPickerState({
    @Default(Status.initialize) Status status,
    Map<String, List<RelationModel>>? relations,
    String? selectedRelation,
  }) = _RelationPickerState;
}
