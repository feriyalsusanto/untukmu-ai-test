part of '../screens/test_screen.dart';

class Content extends StatelessWidget {
  const Content({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8.w, 20.h, 8.w, 0),
      child: Column(
        children: [
          const Divider(),
          AppSpacing.vertical12,
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                'Top 3\nHeroes.',
                style: GoogleFonts.spaceGrotesk(
                    fontSize: 26.sp, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 0.4.sw,
                child: Text(
                  'Take a beat to honor your unsung heroes of the year—those who\'ve been your rocks and rays of light.',
                  style: TextStyle(fontSize: 10.sp),
                ),
              ),
            ],
          ),
          AppSpacing.vertical24,
          Expanded(
            child: BlocBuilder<TestCubit, TestState>(
              builder: (context, state) {
                return ListView.separated(
                    itemBuilder: (context, index) {
                      HeroModel hero = state.heroes![index];
                      return HeroItem(
                        index: index,
                        hero: hero,
                        onPictureTap: () =>
                            context.read<TestCubit>().getPicture(index),
                        onRelationTap: () =>
                            showRelationPicker(context, index, hero.relations),
                      );
                    },
                    separatorBuilder: (context, index) => const Divider(),
                    itemCount: state.heroes?.length ?? 0);
              },
            ),
          )
        ],
      ),
    );
  }

  void showRelationPicker(
      BuildContext context, int index, String? relation) async {
    String? result = await showDialog(
      context: context,
      builder: (context) => RelationPickerDialog(selectedRelation: relation),
    );

    if (result != null) {
      context.read<TestCubit>().update(index, relations: result);
    }
  }
}
