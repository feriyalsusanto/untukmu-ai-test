part of '../screens/test_screen.dart';

class Footer extends StatelessWidget {
  const Footer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.w),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            style: TextButton.styleFrom(
                backgroundColor: AppColors.primaryTextBlackColor,
                padding: EdgeInsets.symmetric(horizontal: 24.w, vertical: 8.h)),
            onPressed: () {},
            child: Row(
              children: [
                Assets.icons.icArrowLeft.image(width: 20.w),
                AppSpacing.horizontal10,
                const Text('Back', style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
          TextButton(
            style: TextButton.styleFrom(
                backgroundColor: AppColors.orange,
                padding: EdgeInsets.symmetric(horizontal: 24.w, vertical: 8.h)),
            onPressed: () {},
            child: Row(
              children: [
                const Text('Next', style: TextStyle(color: Colors.white)),
                AppSpacing.horizontal10,
                Assets.icons.icArrowRight.image(width: 20.w),
              ],
            ),
          ),
        ],
      ),
    );
  }
}