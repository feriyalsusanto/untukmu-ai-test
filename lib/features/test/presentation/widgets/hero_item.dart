part of '../screens/test_screen.dart';

class HeroItem extends StatelessWidget {
  const HeroItem({
    super.key,
    required this.index,
    required this.hero,
    required this.onPictureTap,
    required this.onRelationTap,
  });

  final int index;
  final HeroModel hero;
  final VoidCallback onPictureTap;
  final VoidCallback onRelationTap;

  @override
  Widget build(BuildContext context) {
    var itemWidth = (1.sw - (8.w * 6));

    return SizedBox(
      width: itemWidth,
      height: itemWidth / 2 - 16.h,
      child: Flex(
        direction: Axis.horizontal,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            width: itemWidth / 2 - 8.w,
            child: Stack(
              children: [
                ClipPath(
                  clipper: ItemBackgroundClipper(),
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: BoxDecoration(
                      color: AppColors.grey,
                      image: hero.photoPath != null
                          ? DecorationImage(
                              fit: BoxFit.cover,
                              image: FileImage(File(hero.photoPath!)))
                          : null,
                    ),
                    child: Visibility(
                      visible: hero.photoPath == null,
                      child: Center(
                        child: Text(
                          'Add Photo',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 10.sp,
                              color: AppColors.primaryTextBlackColor
                                  .withOpacity(0.5)),
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: IconButton.filled(
                      onPressed: onPictureTap,
                      iconSize: 20.w,
                      icon: const Icon(Icons.add)),
                ),
              ],
            ),
          ),
          SizedBox(
            width: itemWidth / 2 - 8.w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('Hero ${index + 1}',
                    style: TextStyle(
                        fontWeight: FontWeight.w600, fontSize: 16.sp)),
                AppSpacing.vertical14,
                TextField(
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                        horizontal: 16.w,
                        vertical: 8.h,
                      ),
                      border: defaultBorder,
                      enabledBorder: defaultBorder,
                      fillColor: Colors.white,
                      filled: true,
                      hintText: 'Add Nickname',
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 11.sp,
                          color: Colors.black.withOpacity(0.5))),
                  keyboardType: TextInputType.name,
                  textCapitalization: TextCapitalization.words,
                ),
                AppSpacing.vertical8,
                GestureDetector(
                  onTap: onRelationTap,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 16.w,
                      vertical: 10.h,
                    ),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100.r),
                        border:
                            Border.all(color: Colors.black.withOpacity(0.25))),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            hero.relations == null
                                ? 'Choose Relations'
                                : hero.relations!,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 11.sp,
                                color: Colors.black.withOpacity(
                                    hero.relations == null ? 0.5 : 1)),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        ),
                        AppSpacing.horizontal8,
                        Assets.icons.arrowCircleDown.svg(
                            width: 18.w,
                            colorFilter: ColorFilter.mode(
                                Colors.black.withOpacity(0.25),
                                BlendMode.srcIn)),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  InputBorder get defaultBorder => OutlineInputBorder(
      borderSide: BorderSide(color: Colors.black.withOpacity(0.25)),
      borderRadius: BorderRadius.circular(100.r));
}
