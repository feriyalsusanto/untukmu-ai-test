part of '../screens/test_screen.dart';

class RelationPickerDialog extends StatelessWidget {
  const RelationPickerDialog({super.key, this.selectedRelation});

  final String? selectedRelation;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => picker.RelationPickerCubit()..initialize(),
      child:
          BlocConsumer<picker.RelationPickerCubit, picker.RelationPickerState>(
        listener: (context, state) {
          if (state.status == picker.Status.selected) {
            Navigator.pop(context, state.selectedRelation);
          }
        },
        builder: (context, state) {
          return Dialog(
            insetPadding: EdgeInsets.all(16.w),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24.r)),
            backgroundColor: Colors.white,
            surfaceTintColor: Colors.white,
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.all(24.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Choose Relation',
                    style: TextStyle(fontSize: 18.sp),
                  ),
                  AppSpacing.vertical16,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: List.generate(
                      state.relations?.length ?? 0,
                      (index) {
                        Map<String, List<RelationModel>> data =
                            state.relations!;
                        String title = data.keys.elementAt(index);
                        List<RelationModel> relations = data[title] ?? [];

                        return Container(
                          margin: EdgeInsets.only(bottom: 16.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Text(
                                title,
                                style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12.sp),
                              ),
                              AppSpacing.vertical8,
                              Wrap(
                                spacing: 4.w,
                                runSpacing: 4.w,
                                children:
                                    List.generate(relations.length, (index) {
                                  RelationModel relation = relations[index];
                                  bool selected =
                                      selectedRelation == relation.name;

                                  return TextButton(
                                    onPressed: () => context
                                        .read<picker.RelationPickerCubit>()
                                        .selectItem(relation),
                                    style: TextButton.styleFrom(
                                      foregroundColor: selected
                                          ? Colors.white
                                          : AppColors.primaryTextBlackColor
                                              .withOpacity(0.75),
                                      backgroundColor: selected
                                          ? Theme.of(context).primaryColor
                                          : AppColors.grey2.withOpacity(0.2),
                                    ),
                                    child: Text(
                                      relation.name,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 12.sp),
                                    ),
                                  );
                                }),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
