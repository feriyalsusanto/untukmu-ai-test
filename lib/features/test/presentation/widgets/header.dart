part of '../screens/test_screen.dart';

class Header extends StatelessWidget {
  const Header({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Assets.icons.icLogo.svg(width: 0.115.sw),
        AppSpacing.horizontal0125sw,
        Expanded(
          child: Container(
            margin: EdgeInsets.only(top: 8.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Step 1',
                        style: GoogleFonts.spaceGrotesk(
                            fontWeight: FontWeight.bold,
                            color: Colors.black.withOpacity(0.5))),
                    Text('Reflection',
                        style: TextStyle(
                            fontSize: 22.sp,
                            color: AppColors.primaryTextBlackColor)),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(right: 20.w),
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.w, vertical: 4.h),
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColors.grey),
                      borderRadius: BorderRadius.circular(60.r)),
                  child: Text('01/4',
                      style: TextStyle(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w600,
                      )),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}