import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:untukmu_ai_mobile/common/app_spacing.dart';
import 'package:untukmu_ai_mobile/common/styles/app_colors.dart';
import 'package:untukmu_ai_mobile/common/styles/background_custom_painter.dart';
import 'package:untukmu_ai_mobile/common/styles/item_background_clipper.dart';
import 'package:untukmu_ai_mobile/features/test/bloc/test_cubit.dart';
import 'package:untukmu_ai_mobile/features/test/bloc/widgets/relation_picker/relation_picker_cubit.dart'
    as picker;
import 'package:untukmu_ai_mobile/features/test/data/models/hero/hero_model.dart';
import 'package:untukmu_ai_mobile/features/test/data/models/relation/relation_model.dart';
import 'package:untukmu_ai_mobile/gen/assets.gen.dart';

part '../widgets/content.dart';
part '../widgets/footer.dart';
part '../widgets/header.dart';
part '../widgets/hero_item.dart';
part '../widgets/relation_picker_dialog.dart';

class TestScreen extends StatefulWidget {
  const TestScreen({super.key});

  @override
  State<TestScreen> createState() => _TestScreenState();
}

class _TestScreenState extends State<TestScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => TestCubit()..initialize(),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Expanded(
              child: Padding(
                padding: EdgeInsets.fromLTRB(8.w, kToolbarHeight, 8.w, 8.w),
                child: CustomPaint(
                  size: Size(1.sw, (1.sw * 1.6115384615384614).toDouble()),
                  painter: BackgroundPainter(),
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 8.w, vertical: 16.h),
                    child: const Column(
                      children: [
                        Header(),
                        Expanded(
                          child: Content(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            const Footer(),
          ],
        ),
      ),
    );
  }
}
