import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryTextBlackColor = Color(0xFF24282D);

  static const Color backgroundColor = Color(0xFFF1F1F1);

  static const Color orange = Color(0xFFF6AE70);

  static const Color grey = Color(0xFFCDCDCD);
  static const Color grey2 = Color(0xFF858A9A);

  static const Color darkerGrey = Color(0xFF292D32);
}
