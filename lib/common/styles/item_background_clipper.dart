import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class ItemBackgroundClipper extends CustomClipper<Path> {
  @override
  ui.Path getClip(ui.Size size) {
    Path path_0 = Path();

    // Rounded top left
    path_0.moveTo(size.width * 0.08000000, 0);
    path_0.cubicTo(size.width * 0.03581720, 0, 0, size.height * 0.05969533, 0,
        size.height * 0.1333333);

    // Rounded bottom left
    path_0.lineTo(0, size.height * 0.8666667);
    path_0.cubicTo(0, size.height * 0.9403033, size.width * 0.03581720,
        size.height, size.width * 0.08000000, size.height);

    // Abstract bottom right
    path_0.lineTo(size.width * 0.6130178, size.height);
    path_0.quadraticBezierTo(size.width * 0.7130178, size.height,
        size.width * 0.69, size.height * 0.9230769);
    path_0.cubicTo(
        size.width * 0.6542012,
        size.height * 0.7304487,
        size.width * 0.7825444,
        size.height * 0.6375000,
        size.width * 0.8846154,
        size.height * 0.6594872);
    path_0.quadraticBezierTo(size.width * 0.9704142, size.height * 0.6610897,
        size.width, size.height * 0.6230769);

    // Rounded top right
    path_0.lineTo(size.width, size.height * 0.1333333);
    path_0.cubicTo(size.width, size.height * 0.05969533, size.width * 0.9641820,
        0, size.width * 0.9200000, 0);
    path_0.lineTo(size.width * 0.08000000, 0);

    path_0.close();

    return path_0;
  }

  @override
  bool shouldReclip(covariant CustomClipper<ui.Path> oldClipper) => true;
}
