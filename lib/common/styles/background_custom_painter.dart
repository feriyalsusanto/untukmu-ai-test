import 'package:flutter/material.dart';

class BackgroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) async {
    Path path_0 = Path();
    path_0.moveTo(size.width, size.height * 0.04773270);
    path_0.cubicTo(size.width, size.height * 0.02137064, size.width * 0.9655577,
        0, size.width * 0.9230769, 0);
    path_0.lineTo(size.width * 0.2693231, 0);
    path_0.cubicTo(
        size.width * 0.2268394,
        0,
        size.width * 0.1924000,
        size.height * 0.02137064,
        size.width * 0.1924000,
        size.height * 0.04773270);
    path_0.lineTo(size.width * 0.1924000, size.height * 0.07159905);
    path_0.cubicTo(
        size.width * 0.1924000,
        size.height * 0.09796122,
        size.width * 0.1579606,
        size.height * 0.1193317,
        size.width * 0.1154769,
        size.height * 0.1193317);
    path_0.lineTo(size.width * 0.07692308, size.height * 0.1193317);
    path_0.cubicTo(size.width * 0.03443962, size.height * 0.1193317, 0,
        size.height * 0.1407023, 0, size.height * 0.1670644);
    path_0.lineTo(0, size.height * 0.9522673);
    path_0.cubicTo(0, size.height * 0.9786277, size.width * 0.03443962,
        size.height, size.width * 0.07692308, size.height);
    path_0.lineTo(size.width * 0.9230769, size.height);
    path_0.cubicTo(size.width * 0.9655577, size.height, size.width,
        size.height * 0.9786277, size.width, size.height * 0.9522673);
    path_0.lineTo(size.width, size.height * 0.04773270);
    path_0.close();

    Paint paint0Fill = Paint()..style = PaintingStyle.fill;
    paint0Fill.color = const Color(0xffF1F1F1).withOpacity(1.0);
    canvas.drawPath(path_0, paint0Fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
