import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppSpacing {
  static final SizedBox horizontal4 = SizedBox(width: 4.w);
  static final SizedBox horizontal8 = SizedBox(width: 8.w);
  static final SizedBox horizontal10 = SizedBox(width: 10.w);
  static final SizedBox horizontal12 = SizedBox(width: 12.w);
  static final SizedBox horizontal16 = SizedBox(width: 16.w);
  static final SizedBox horizontal32 = SizedBox(width: 32.w);

  static final SizedBox horizontal01sw = SizedBox(width: 0.1.sw);
  static final SizedBox horizontal0125sw = SizedBox(width: 0.125.sw);

  static final SizedBox vertical4 = SizedBox(height: 4.h);
  static final SizedBox vertical8 = SizedBox(height: 8.h);
  static final SizedBox vertical10 = SizedBox(height: 10.h);
  static final SizedBox vertical12 = SizedBox(height: 12.h);
  static final SizedBox vertical14 = SizedBox(height: 14.h);
  static final SizedBox vertical16 = SizedBox(height: 16.h);
  static final SizedBox vertical24 = SizedBox(height: 24.h);
  static final SizedBox vertical32 = SizedBox(height: 32.h);

  static final SizedBox vertical01sh = SizedBox(height: 0.1.sh);
  static final SizedBox vertical0125sh = SizedBox(height: 0.125.sh);
}
